self: super:
let
  hlib = super.haskell.lib;
  lib = super.lib;
  gitignore = path:
    super.nix-gitignore.gitignoreSourcePure [ (path + /.gitignore) ] path;
  zetelpatatOverrides = selfh: superh: {
    zetelpatat =
      superh.callCabal2nix "zetelpatat" (gitignore ./.) { };
  };
in {
  haskellPackages = super.haskellPackages.override (old: {
    overrides =
      lib.composeExtensions (old.overrides or (_: _: { })) zetelpatatOverrides;
  });
}
