let
  pkgs = import ./prelude.nix;

  overlays = [(import ./overlay.nix)];
  nixpkgs-overlayed = pkgs { inherit overlays; };

in { inherit (nixpkgs-overlayed.haskellPackages) zetelpatat; }

