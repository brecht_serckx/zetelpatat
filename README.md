# zetelpatat

## Installation instructions


## To Do:

- [x] fix the url passing
- [x] print episode urls on query
- [x] config file may not exist
- [x] config file may exists, but be invalid
- [x] decent App config needs to be made
- [x] available config items need to be added to App config
- [x] first load config file, then overwrite with cli args
- [x] cli args are optional
- [x] youtube-dl args in config and opts
- [x] custom download directory support
- [x] use RIO instead of Prelude
- [x] three layer haskell cake?
- [x] logging/debugging
- [x] find a better/more intuitive way for cli args
- [x] improve structure of indexer with filters instead of the current mess
- [x] mtl
- [x] use Text instead of String
- [x] fix bugs in downloading: it now downloads all episodes, instead of the matched
- [x] structure
- [ ] use exceptions
      - [x] ExceptT stack
      - [ ] Catch IO exceptions
      - [x] Think about exceptions: NotFoundError, ParseError, ... and put those in one datatype
            -> create a hierarchy of errors:
               - NotFoundError:
                 - ProvidersNotFoundError (Constraint Provider)
                 - ShowNotFoundError (Constraint TVShow)
                 - ...
               - ParseError:
                 - NoShowUrlError LByteString
                 - NoSeasonTitleError TVShow LByteString
                 - NoSeasonURLError TVShow LByteString
- [x] scrape episode index: do this by scraping extended episode info from episode html
- [ ] others websites than vrtnu? (vier, vijf, mtv, ...)
      - [x] fix framework using providers
      - [ ] implement vier provider
      - [ ] implement vtm provider
      - [ ] implement vijf provider
      - [ ] implement mtv provider
- [ ] keep list of tracked shows
- [ ] simplify zetelpatat
      - [ ] add `describe` endpoint that outputs json information: id, title, description, ...
      - [ ] add a script that downloads all of these using youtube-dl and jq
      - [ ] update the code 
- [ ] use proper regexes for all keywords and constraints
- [ ] fix unicode handling (case: Beau Sejour)

## API

### General

    zetelpatat [OPTIONS] COMMAND [COMMAND_OPTIONS]

List all items that match the query, while fulfilling the constraints.

Options:

- `-h`: show help
- `-u`: username
- `-p`: password
- `-c`: config file location
- `-C`: youtube-dl config file location
- `-A`: youtube-dl additional arguments
- `-d`: directory to store downloads
- `-i`: show extended information about items (TODO)

### Commands

#### List

    list QUERY [CONSTRAINTS]

Lists all items of the query satisfying the constraints.
Queries:

- `show`: list shows
- `season`: list seasons
- `episode`: list episodes

List all providers:

    zetelpatat list provider

List all shows:

    zetelpatat list show

List all seasons of a show:

    zetelpatat list season -s SHOW

List all episodes of a season of a show:

    zetelpatat list episode -s SHOW -s SEASON

List all episodes of a show:

    zetelpatat list episode -s SHOW

### Searching

    search QUERY [CONSTRAINTS] KEYWORDS

List all items that match the query and keywords, while fullfilling the constraints.
Queries:

- `show`: list shows
- `season`: list seasons
- `episode`: list episodes

Search a show:

    zetelpatat search show KWS

Search a season of a show:

    zetelpatat search season -s SHOW KWS

Search an episode of a season of a show:

    zetelpatat search episode -s SHOW -t SEASON KWS

Search an episode of a show:

    zetelpatat search episode -s SHOW KWS

### Download

    download QUERY [CONSTRAINTS] KWS [-- [YOUTUBE-DL-ARGS]]

Download all items that match the query and keywords, while fulfilling the constraints.
Queries:

- `show`: list shows
- `season`: list seasons
- `episode`: list episodes

Search a show:

    zetelpatat search show KWS

Search a season of a show:

    zetelpatat search season -s SHOW KWS

Search an episode of a season of a show:

    zetelpatat search episode -s SHOW -t SEASON KWS

Search an episode of a show:

    zetelpatat search episode -s SHOW KWS

Download a show:

    zetelpatat download show KWS

Download a season of a show:

    zetelpatat download season -s SHOW KWS

Download an episode of a season of a show:

    zetelpatat download episode -s SHOW -t SEASON KWS

Download an episode of a show:

    zetelpatat download episode -s SHOW KWS

## Configuration file

Needs to contain:

- section ZetelPatat
  - username
  - password
- section YoutubeDL
  - path to youtube-downloader config file
  - additional youtube-dl arguments
