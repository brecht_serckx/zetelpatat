module Control.Monad.Http
( MonadHttp(..)
, LByteString(..)
, Url(..)
, BS.unpack
)
where


import           RIO

import qualified Data.ByteString.Lazy.Char8 as BS (unpack)

class Monad m => MonadHttp m where
  get :: Url -> m LByteString

type Url = Text
