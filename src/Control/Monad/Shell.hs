module Control.Monad.Shell where

import           RIO
import qualified Shelly as S (FilePath)

class Monad m => MonadShell m where
  runShell :: S.FilePath -> [Text] -> m ()
  mkdir    :: Text -> m ()
  setBufferMode :: m ()
