{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell   #-}
module Main where

import           RIO                   hiding (lofWarn, log, logDebug, logError,
                                        logInfo, logOther)
import           RIO.Text              (intercalate)

import           Control.Monad.Except  (runExceptT)
import           Control.Monad.Logger
import           System.IO             (print, putStrLn)

import           ZetelPatat.App        (App, AppT, createApp)
import           ZetelPatat.Opts       (Command (..), Opts (..), Query (..),
                                        parseOpts)
import           ZetelPatat.Types
import           ZetelPatat.ZetelPatat

appName :: Text
appName = "zetelpatat"


main :: IO ()
main = do
  opts <- parseOpts
  app <- createApp appName opts
  res <- runExceptT $ flip runReaderT app $ execCommand $ optCommand opts
  case res of
    Left e  -> putStrLn $ "Error: " <> show e
    Right s -> pure ()


execCommand :: Command -> AppT ()
execCommand cmd = do
  $logDebug $ "Executing command: " <> tshow cmd
  case cmd of
    ListCommand q       -> execList q
    SearchCommand q kws -> execSearch q kws
    SyncCommand q kws   -> execSync q kws

execList :: Query -> AppT ()
execList q =
  case q of
    ProviderQuery         -> liftIO $ mapM_ print listProviders
    ShowQuery pc          -> listShowsCt pc >>= liftIO . mapM_ print
    SeasonQuery pc sc     -> listSeasonsCt pc sc >>= liftIO . mapM_ print
    EpisodeQuery pc sc tc -> listEpisodesCt pc sc tc >>= liftIO . mapM_ print

execSearch :: Query -> [Text] -> AppT ()
execSearch q kws =
  case q of
    ProviderQuery         -> liftIO . mapM_ print $ searchProviders kws
    ShowQuery pc          -> searchShows pc kws >>= liftIO . mapM_ print
    SeasonQuery pc sc     -> searchSeasons pc sc kws >>= liftIO . mapM_ print
    EpisodeQuery pc sc tc -> searchEpisodes pc sc tc kws >>= liftIO . mapM_ print

execSync :: Query -> [Text] -> AppT ()
execSync q kws = case q of
    EpisodeQuery pc sc tc -> downloadEpisode pc sc tc $ map ExactMatch kws
    SeasonQuery pc sc     -> downloadSeason pc sc $ map ExactMatch kws
    ShowQuery pc          -> downloadShow pc $ map ExactMatch kws
    ProviderQuery         -> downloadProvider $ map ExactMatch kws
