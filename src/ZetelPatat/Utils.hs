module ZetelPatat.Utils
where

import           RIO
import           RIO.List         (isInfixOf, nub, (\\))
import           RIO.Text         (Text)
import qualified RIO.Text         as T

import           ZetelPatat.Types


-- | Determine whether a string contains all of a list of substrings
match :: [Text] -- ^ Substrings
      -> Text   -- ^ Superstring
      -> Bool   -- ^ Result
match subs super =
  let
    contains :: Text -> Text -> Bool
    contains sub super = T.isInfixOf (T.toUpper sub) (T.toUpper super)
  in
    all (`contains` super) subs

-- | Filter all elements which fullfull a constraint
filterCtOn :: HasName a
           => Constraint a -- ^ Constraint
           -> (e -> a) -- ^ Reader
           -> [e] -- ^ List to filter
           -> [e] -- ^ Filtered list
filterCtOn mc f =
  case mc of
    Wildcard     -> id
    ExactMatch c -> filter ((==c) . name . f)

filterCt :: HasName a
         => Constraint a -- ^ Constraint
         -> [a] -- ^ List to filter
         -> [a] -- ^ Filtered list
filterCt mc = filterCtOn mc id

filterSQOn :: SearchQuery -> (a -> Text) -> [a] -> [a]
filterSQOn sq f = filter (match sq . f)

concatMapM :: (Traversable t, Monad m)
           => (a -> m [b])
           -> t a
           -> m [b]
concatMapM f xs = concat <$> mapM f xs

concatForM :: (Traversable t, Monad m)
           => t a
           -> (a -> m [b])
           -> m [b]
concatForM = flip concatMapM
