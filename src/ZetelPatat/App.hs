{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}
module ZetelPatat.App
( App(..)
, AppT
, createApp
, liftIO
, ask
, Config(..)
, ZetelPatatConfig(..)
, YoutubeDLConfig(..)
, mkUrl
)
where

import           RIO                     hiding (LogLevel (..), logDebug,
                                          logError, logInfo, logOther, logWarn)
import qualified RIO.Text                as T
import           System.IO               (print)

import           Control.Monad.Except
import           Control.Monad.Http
import           Control.Monad.Logger
import           Control.Monad.Shell
import           Network.HTTP.Client     (Manager, httpLbs, newManager,
                                          parseRequest, responseBody)
import           Network.HTTP.Client.TLS (tlsManagerSettings)
import           Shelly                  hiding (FilePath)
import           System.Directory        (createDirectoryIfMissing,
                                          getCurrentDirectory)

import           ZetelPatat.IniConfig
import           ZetelPatat.Opts
import           ZetelPatat.Types


data App = App
  { appName        :: Text
  , appHttpManager :: Manager
  , appConfig      :: Config
  }

type AppT = ReaderT App (ExceptT AppError IO)

instance MonadHttp AppT where
  get url = do
    app <- ask
    $logInfo $ "Querying url: " <> url
    liftIO $ do
      req <- parseRequest . T.unpack . mkUrl $ url
      response <- httpLbs req $ appHttpManager app
      return . responseBody $ response

instance MonadShell AppT where
  runShell cmd args = void
                    $ shelly
                    $ verbosely
                    $ run cmd args

  mkdir path = liftIO . createDirectoryIfMissing True $ T.unpack path

  setBufferMode = hSetBuffering stdout LineBuffering

instance {-# OVERLAPPING #-} MonadLogger AppT where
  monadLoggerLog loc src lvl msg = do
    verbosity <- cfVerbosity . cfZetelPatat . appConfig <$> ask
    when (lvl >= verbosity) $
      liftIO . print $
        tshow lvl <> " - " <> tshow (toLogStr msg)


data Config = Config
  { cfZetelPatat :: !ZetelPatatConfig
  , cfYoutubeDL  :: !YoutubeDLConfig
  }
  deriving (Eq,Show)

data ZetelPatatConfig = ZetelPatatConfig
  { cfVerbosity :: !LogLevel
  , cfUsername  :: !(Maybe Text)
  , cfPassword  :: !(Maybe Text)
  , cfDirectory :: !FilePath
  }
  deriving (Eq,Show)

data YoutubeDLConfig = YoutubeDLConfig
  { cfConfigFile :: !(Maybe FilePath)
  , cfCmdArgs    :: !(Maybe [Text])
  }
  deriving (Eq,Show)


createApp :: Text -> Opts -> IO App
createApp name opts = do
  let appName = name
  appHttpManager <- newManager tlsManagerSettings
  appConfig <- loadConfig name opts
  return $ App {..}


loadConfig :: Text -> Opts -> IO Config
loadConfig name Opts{..} = do
  IniConfig{..} <- loadIniConfig name optConfig
  curDir <- liftIO getCurrentDirectory
  return Config
    { cfZetelPatat = ZetelPatatConfig
        { cfVerbosity = flip fromMaybe optVerbosity $ icVerbosity icZetelPatat
        , cfUsername = optUsername <|> icUsername icZetelPatat
        , cfPassword = optPassword <|> icPassword icZetelPatat
        , cfDirectory = fromMaybe curDir $ optDirectory <|> icDirectory icZetelPatat
        }
    , cfYoutubeDL = YoutubeDLConfig
        { cfConfigFile = optYDLConfig <|> icConfigFile icYoutubeDL
        , cfCmdArgs = optYDLArgs <|> icCmdArgs icYoutubeDL
        }
    }


mkUrl :: Url -> Url
mkUrl url = "https://www.vrt.be" <> url
