{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
module ZetelPatat.IniConfig where

import           RIO                  hiding (LogLevel (..))
import qualified RIO.Text             as T

import           Control.Monad.Logger (LogLevel (..))
import           Data.Ini.Config      (IniParser, fieldDefOf, fieldMbOf,
                                       parseIniFile, readable, section, string)
import           System.Directory     (XdgDirectory (..), doesFileExist,
                                       getXdgDirectory)

configFileName :: Text
configFileName = "config.ini"


data IniConfig = IniConfig
  { icZetelPatat   :: !ZetelPatatIniConfig
  , icYoutubeDL :: !YoutubeDLIniConfig
  }
  deriving (Eq,Show)

data ZetelPatatIniConfig = ZetelPatatIniConfig
  { icVerbosity :: !LogLevel
  , icUsername  :: !(Maybe Text)
  , icPassword  :: !(Maybe Text)
  , icDirectory :: !(Maybe FilePath)
  }
  deriving (Eq,Show)

data YoutubeDLIniConfig = YoutubeDLIniConfig
  { icConfigFile :: !(Maybe FilePath)
  , icCmdArgs    :: !(Maybe [Text])
  }
  deriving (Eq,Show)



loadIniConfig :: Text -> Maybe FilePath -> IO IniConfig
loadIniConfig name mPath
  = case mPath of
    Just path -> do
      b <- doesFileExist path
      if b
        then loadIniConfigFromFile path
        else error $ "Config file not found: " <> path
    Nothing -> do
      configDir <- getXdgDirectory XdgConfig $ T.unpack name
      let path =  configDir <> "/" <> T.unpack configFileName
      b <- doesFileExist path
      if b
        then loadIniConfigFromFile path
        else return loadDefaultIniConfig


loadDefaultIniConfig :: IniConfig
loadDefaultIniConfig = IniConfig
  { icZetelPatat = ZetelPatatIniConfig
    { icVerbosity = LevelError
    , icUsername = Nothing
    , icPassword = Nothing
    , icDirectory = Nothing
    }
  , icYoutubeDL = YoutubeDLIniConfig
    { icConfigFile = Nothing
    , icCmdArgs = Nothing
    }
  }

loadIniConfigFromFile :: FilePath -> IO IniConfig
loadIniConfigFromFile path = do
  file <- readFileUtf8 path
  case parseIniFile file configParser of
    Left msg  -> error msg
    Right cfg -> return cfg


configParser :: IniParser IniConfig
configParser = do
  icZetelPatat <- section "ZetelPatat" $ do
    icVerbosity <- fieldDefOf "verbosity" readable LevelError
    icUsername <- fieldMbOf "username" string
    icPassword <- fieldMbOf "password" string
    icDirectory <- fieldMbOf "directory" string
    return ZetelPatatIniConfig {..}
  icYoutubeDL <- section "YoutubeDL" $ do
    icConfigFile <- fieldMbOf "config_file" string
    icCmdArgs <- fmap T.words <$> fieldMbOf "config_file" string
    return YoutubeDLIniConfig {..}
  return IniConfig {..}
