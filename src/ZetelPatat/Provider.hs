{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE ScopedTypeVariables       #-}
module ZetelPatat.Provider where

import           RIO              hiding (logDebug, logError, logInfo, logOther,
                                   logWarn, unpack)


import           ZetelPatat.Types
import           ZetelPatat.Utils
