{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE LambdaCase          #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections       #-}
module ZetelPatat.ZetelPatat where

import           RIO                     hiding ( logDebug
                                                , logError
                                                , logInfo
                                                , logOther
                                                , logWarn
                                                , unpack
                                                )
import           RIO.List                       ( nub
                                                , (\\)
                                                )

import           Control.Monad.Except
import           Control.Monad.Http             ( MonadHttp )
import           Control.Monad.Logger
import           Control.Monad.Shell            ( MonadShell )

import           ZetelPatat.App                 ( App )
import qualified ZetelPatat.Download           as D
import           ZetelPatat.Types               ( AProvider(..)
                                                , AppError(..)
                                                , Constraint
                                                , Episode(..)
                                                , Provider(..)
                                                , SearchQuery
                                                , Season(..)
                                                , TVShow(..)
                                                , aProviderName
                                                )
import qualified ZetelPatat.Types              as P
import           ZetelPatat.Utils

import           ZetelPatat.Provider.VrtNu      ( VrtNuProvider(..) )


{-
 - List
 -}

-- | List all providers
listProviders :: [AProvider]
listProviders = [AProvider VrtNuProvider]

-- | List all shows according to provider constraints
listShowsCt
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> m [(AProvider, TVShow)]
listShowsCt pc = concatMapM (\ap@(AProvider p) -> map (ap, ) <$> P.listShows p)
  =<< findProviders pc

-- | List all seasons according to provider and
listSeasonsCt
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> m [(AProvider, Season)]
listSeasonsCt pc sc = do
  pss <- findShows pc sc
  concatMapM (\(ap@(AProvider p), s) -> map (ap, ) <$> P.listSeasons p s) pss

-- | List all episodes conforming to constraints
listEpisodesCt
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> Constraint Season
  -> m [(AProvider, Episode)]
listEpisodesCt pc sc tc = do
  pts <- findSeasons pc sc tc
  concatMapM (\(ap@(AProvider p), t) -> map (ap, ) <$> P.listEpisodes p t) pts



{-
 - Find
 -}

-- | Find providers that fullfill constraints
findProviders :: MonadError AppError m => Constraint AProvider -> m [AProvider]
findProviders = findProviders' . pure

findProviders'
  :: MonadError AppError m => [Constraint AProvider] -> m [AProvider]
findProviders' pcs =
  let ps = listProviders
      es = map
        (\pc ->
          \case
              [] -> Left pc
              ps -> Right ps
            . filterCt pc
            $ ps
        )
        pcs
  in  case partitionEithers es of
        ([], rs) -> return $ concat rs
        (ls, _ ) -> throwError $ ProvidersNotFoundError ls

findShows
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> m [(AProvider, TVShow)]
findShows pc = findShows' pc . pure

findShows'
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> [Constraint TVShow]
  -> m [(AProvider, TVShow)]
findShows' pc scs = do
  ss <- listShowsCt pc
  let es = map
        (\sc ->
          \case
              [] -> Left sc
              ss -> Right ss
            . filterCtOn sc snd
            $ ss
        )
        scs
  case partitionEithers es of
    ([], rs) -> return $ concat rs
    (ls, _ ) -> throwError $ ShowsNotFoundError ls

findSeasons
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> Constraint Season
  -> m [(AProvider, Season)]
findSeasons pc sc = findSeasons' pc sc . pure

findSeasons'
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> [Constraint Season]
  -> m [(AProvider, Season)]
findSeasons' pc sc tcs = do
  ts <- listSeasonsCt pc sc
  let es = map
        (\tc ->
          \case
              [] -> Left tc
              ss -> Right ss
            . filterCtOn tc snd
            $ ts
        )
        tcs
  case partitionEithers es of
    ([], rs) -> return $ concat rs
    (ls, _ ) -> throwError $ SeasonsNotFoundError ls

findEpisodes
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> Constraint Season
  -> Constraint Episode
  -> m [(AProvider, Episode)]
findEpisodes pc sc tc = findEpisodes' pc sc tc . pure

findEpisodes'
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> Constraint Season
  -> [Constraint Episode]
  -> m [(AProvider, Episode)]
findEpisodes' pc sc tc ecs = do
  es <- listEpisodesCt pc sc tc
  let es' = map
        (\ec ->
          \case
              [] -> Left ec
              ss -> Right ss
            . filterCtOn ec snd
            $ es
        )
        ecs
  case partitionEithers es' of
    ([], rs) -> return $ concat rs
    (ls, _ ) -> throwError $ EpisodesNotFoundError ls



{-
 - Search
 -}

-- | Search providers
searchProviders :: SearchQuery -> [AProvider]
searchProviders sq = filterSQOn sq aProviderName listProviders

-- | Search shows
searchShows
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> SearchQuery
  -> m [TVShow]
searchShows pc sq = filterSQOn sq showName . map snd <$> listShowsCt pc

-- | Search seasons
searchSeasons
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> SearchQuery
  -> m [Season]
searchSeasons pc sc sq =
  filterSQOn sq seasonName . map snd <$> listSeasonsCt pc sc

-- | Search Episodes
searchEpisodes
  :: (MonadError AppError m, MonadHttp m)
  => Constraint AProvider
  -> Constraint TVShow
  -> Constraint Season
  -> SearchQuery
  -> m [Episode]
searchEpisodes pc sc tc sq =
  filterSQOn sq episodeName . map snd <$> listEpisodesCt pc sc tc



{-
 - Download
 -}

-- | Download everything from a provider
downloadProvider
  :: ( MonadError AppError m
     , MonadReader App m
     , MonadLogger m
     , MonadShell m
     , MonadHttp m
     )
  => [Constraint AProvider]
  -> m ()
downloadProvider pcs = mapM_ D.downloadEpisode =<< do
  ps <- findProviders' pcs
  concatForM ps $ \(AProvider p) ->
    concatMapM (P.listEpisodes p)
      =<< concatMapM (P.listSeasons p)
      =<< P.listShows p

-- | Download shows
downloadShow
  :: ( MonadError AppError m
     , MonadReader App m
     , MonadLogger m
     , MonadShell m
     , MonadHttp m
     )
  => Constraint AProvider
  -> [Constraint TVShow]
  -> m ()
downloadShow pc scs = mapM_ D.downloadEpisode =<< do
  pss <- findShows' pc scs
  concatForM pss
    $ \(AProvider p, s) -> concatMapM (P.listEpisodes p) =<< P.listSeasons p s

-- | Download seasons
downloadSeason
  :: ( MonadError AppError m
     , MonadReader App m
     , MonadLogger m
     , MonadShell m
     , MonadHttp m
     )
  => Constraint AProvider
  -> Constraint TVShow
  -> [Constraint Season]
  -> m ()
downloadSeason pc sc tcs = mapM_ D.downloadEpisode =<< do
  pts <- findSeasons' pc sc tcs
  concatForM pts $ \(AProvider p, t) -> P.listEpisodes p t

-- | Download episodes
downloadEpisode
  :: ( MonadError AppError m
     , MonadReader App m
     , MonadLogger m
     , MonadShell m
     , MonadHttp m
     )
  => Constraint AProvider
  -> Constraint TVShow
  -> Constraint Season
  -> [Constraint Episode]
  -> m ()
downloadEpisode pc sc tc ecs =
  mapM_ (D.downloadEpisode . snd) =<< findEpisodes' pc sc tc ecs
