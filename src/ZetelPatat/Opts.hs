{-# LANGUAGE OverloadedStrings #-}
module ZetelPatat.Opts where

import           RIO                  hiding (LogLevel (..))
import qualified RIO.Text             as T

import           Control.Monad.Logger (LogLevel (..))
import           Options.Applicative  hiding (HasName)

import           ZetelPatat.Types


data Opts = Opts
  { optVerbosity :: Maybe LogLevel
  , optUsername  :: Maybe Text
  , optPassword  :: Maybe Text
  , optConfig    :: Maybe FilePath
  , optYDLConfig :: Maybe FilePath
  , optYDLArgs   :: Maybe [Text]
  , optDirectory :: Maybe FilePath
  , optCommand   :: Command
  }

data Command
  = ListCommand
    { cmdQuery    :: Query
    }
  | SearchCommand
    { cmdQuery    :: Query
    , cmdKeywords :: SearchQuery
    }
  | SyncCommand
    { cmdQuery    :: Query
    , cmdKeywords :: [Text]
    }
  deriving (Eq, Show)

data Query
  = ProviderQuery
  | ShowQuery
  { showConstrainstProvider :: Constraint AProvider
  }
  | SeasonQuery
  { seasonConstrainstProvider :: Constraint AProvider
  , seasonConstraintShow      :: Constraint TVShow
  }
  | EpisodeQuery
  { episodeConstrainstProvider :: Constraint AProvider
  , episodeConstraintShow      :: Constraint TVShow
  , episodeConstraintSeason    :: Constraint Season
  }
  deriving (Eq, Show)

parseOpts :: IO Opts
parseOpts = execParser optsParser

optsParser :: ParserInfo Opts
optsParser = info
  ( helper <*> programOptions)
  (  fullDesc
  <> progDesc "zetelpatat"
  <> header "zetelpatat: show grabber for VRT Nu"
 -- <> noIntersperse
  )


programOptions :: Parser Opts
programOptions =
  let
    -- Common options
    -- Verbosity
    verbosityOption = option auto $
      long "verbosity" <> short 'V' <> metavar "VERBOSITY"
      <> help "Verbosity: LevelError, LevelWarn, LevelInfo, LevelDebug, LevelOther TEXT"
    -- Username
    usernameOption = strOption $
      long "username" <> short 'u' <> metavar "USERNAME"  <> help "Username"
    -- Password
    passwordOption = strOption $
      long "password" <> short 'p' <> metavar "PASSWORD"  <> help "Password"
    -- Config file path
    configOption = strOption $
      long "config" <> short 'c' <> metavar "FILE" <> help "Path to zetelpatat config file"
    -- Youtube-DL config file path
    ydlConfigOption = strOption $
      long "youtube-dl-config" <> short 'C' <> metavar "FILE" <> help "Path to youtube-dl config file"
    -- Youtube-DL arguments
    ydlArgsOption = strOption $
      long "youtube-dl-args" <> short 'A' <> metavar "ARGS" <> help "Options passed to youtube-dl"
    -- Target directory
    directoryOption = strOption $
      long "directory" <> short 'd' <> metavar "DIR" <> help "Download videos to this directory"

    -- Commands
    -- List
    listCommand = command "list" $ info listOptions $ progDesc "List shows/seasons/episodes"
    listOptions = ListCommand
      <$> queryParser
    -- Search
    searchCommand = command "search" $ info searchOptions $ progDesc "Search shows/seasons/episodes"
    searchOptions = SearchCommand
      <$> queryParser
      <*> many keywordOption
    -- Sync
    syncCommand = command "sync" $ info syncOptions $ progDesc "Download shows/seasons/episodes"
    syncOptions = SyncCommand
      <$> queryParser
      <*> many keywordOption

    -- Queries
    queryParser = hsubparser (providerQuery <> showQuery <> seasonQuery <> episodeQuery)
    -- Provider
    providerQuery = command "provider" $ info providerQueryOptions $ progDesc "Provider"
    providerQueryOptions = pure ProviderQuery
    -- Show
    showQuery = command "show" $ info showQueryOptions $ progDesc "Show"
    showQueryOptions = ShowQuery
      <$> (mkConstraint <$> optional providerConstraint)
    -- Season
    seasonQuery = command "season" $ info seasonQueryOptions $ progDesc "Season"
    seasonQueryOptions
       =  SeasonQuery
      <$> (mkConstraint <$> optional providerConstraint)
      <*> (mkConstraint <$> optional showConstraint)
    -- Episode
    episodeQuery = command "episode" $ info episodeQueryOptions $ progDesc "Episode"
    episodeQueryOptions
       =  EpisodeQuery
      <$> (mkConstraint <$> optional providerConstraint)
      <*> (mkConstraint <$> optional showConstraint)
      <*> (mkConstraint <$> optional seasonConstraint)

    -- Provider Constraint
    providerConstraint = strOption $
      long "providers" <> short 'p' <> metavar "PROVIDER_NAME" <>
      showDefault <> help "Provider name"
    -- Show Constraint
    showConstraint = strOption $
      long "shows" <> short 's' <> metavar "SHOW_NAME" <>
      showDefault <> help "Show name"
    -- Season Constraint
    seasonConstraint = strOption $
      long "seasons" <> short 't' <> metavar "SEASON_NAME" <>
      showDefault <> help "Season name"
    mkConstraint :: HasName a => Maybe Text -> Constraint a
    mkConstraint = maybe Wildcard ExactMatch

    -- Keywords
    keywordOption = strArgument $ metavar "KEYWORD" <> help "Series/Season/Episode to download/search"
  in
    Opts <$> optional verbosityOption
         <*> optional usernameOption
         <*> optional passwordOption
         <*> optional configOption
         <*> optional ydlConfigOption
         <*> (fmap T.words <$> optional ydlArgsOption)
         <*> optional directoryOption
         <*> hsubparser
               ( listCommand
              <> searchCommand
              <> syncCommand
               )
