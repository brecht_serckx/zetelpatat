{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE TemplateHaskell   #-}
module ZetelPatat.Download
where

import           RIO                  hiding (log, logDebug, logError, logInfo,
                                       logOther, logWarn)
import qualified RIO.Text             as T

import           Control.Monad.Http   (Url (..))
import           Control.Monad.Logger
import           Control.Monad.Shell

import           ZetelPatat.App
import           ZetelPatat.Types


-- | Download an episode
downloadEpisode :: (MonadShell m, MonadReader App m, MonadLogger m)
                => Episode
                -> m ()
downloadEpisode e = do
  let url = episodeUrl e
      t = episodeSeason e
      n = episodeIndex e
      s = seasonShow t
  curDir <- T.pack . cfDirectory . cfZetelPatat . appConfig <$> ask
  let relDir =  T.intercalate "/"
             [ T.empty
             , showName s
             , seasonName t
             , tshow n <> ". " <> episodeName e
             ]
      absDir = curDir <> relDir
  downloadVideo absDir $ mkUrl url

downloadVideo :: (MonadShell m, MonadReader App m, MonadLogger m)
              => Text
              -> Text
              -> m ()
downloadVideo path url = do
  $logInfo $ "Downloading " <> url <> " to " <> path
  Config {..} <- appConfig <$> ask
  setBufferMode
  runShell "youtube-dl"
           $ maybe [] (\s -> ["--username",s]) (cfUsername cfZetelPatat)
          ++ maybe [] (\s -> ["--password",s]) (cfPassword cfZetelPatat)
          ++ [ "-o", path <> ".%(ext)s" ]
          ++ fromMaybe [] (cfCmdArgs cfYoutubeDL)
          ++ [url]
  return ()
