{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE OverloadedStrings         #-}
module ZetelPatat.Types where

import           RIO
import           RIO.List             (intercalate)
import qualified RIO.Text             as T

import           Control.Monad.Except
import           Control.Monad.Http   (MonadHttp, Url (..))

data AppError
  = ProvidersNotFoundError [Constraint AProvider]
  | ShowsNotFoundError [Constraint TVShow]
  | SeasonsNotFoundError [Constraint Season]
  | EpisodesNotFoundError [Constraint Episode]
  | ParseError Text
  deriving (Eq)

instance Show AppError where
  show ae = case ae of
    ProvidersNotFoundError cs -> "Providers not found: " <> intercalate ", " (map show cs)
    ShowsNotFoundError cs     -> "Shows not found: "     <> intercalate ", " (map show cs)
    SeasonsNotFoundError cs   -> "Seasons not found: "   <> intercalate ", " (map show cs)
    EpisodesNotFoundError cs  -> "Episodes not found: "  <> intercalate ", " (map show cs)
    ParseError msg -> "Could not parse html: " <> show msg

class HasName a where
  name :: a -> Text

data Constraint a
  = HasName a
 => ExactMatch Text
  | Wildcard

instance Eq (Constraint a) where
  (ExactMatch t1) == (ExactMatch t2) = t1 == t2
  Wildcard == Wildcard = True
  _ == _ = False

instance Show (Constraint a) where
  show Wildcard          = "*"
  show (ExactMatch text) = show text

type SearchQuery = [Text]


{-
 - Provider
 -}

class (Show p,Eq p) => Provider p where
  -- | Provider name
  providerName  :: p -> Text

  -- | List shows
  listShows    :: (MonadError AppError m, MonadHttp m) => p -> m [TVShow]

  -- | List seasons for a show
  listSeasons    :: (MonadError AppError m, MonadHttp m) => p -> TVShow -> m [Season]

  -- | List episodes for a season
  listEpisodes    :: (MonadError AppError m, MonadHttp m) => p -> Season -> m [Episode]

data AProvider = forall p. Provider p =>
  AProvider
    { unAProvider :: p
    }

instance Show AProvider where
  show (AProvider p) = T.unpack . providerName $ p

instance Eq AProvider where
  (AProvider pa) == (AProvider pb) = providerName pa == providerName pb

instance HasName AProvider where
  name = aProviderName

aProviderName :: AProvider -> Text
aProviderName (AProvider p) = providerName p

data TVShow
  = TVShow
    { showName :: Text
    , showUrl  :: Url
    } deriving (Eq)

instance Show TVShow where
  show (TVShow n u) = show $
    n <> " (" <> u <> ")"

instance HasName TVShow where
  name = showName

data Season
  = Season
    { seasonShow :: TVShow
    , seasonName :: Text
    , seasonUrl  :: Url
    } deriving (Eq)

instance Show Season where
  show (Season s n u) = show $
    showName s <> " > " <> n <> " (" <> u <> ")"

instance HasName Season where
  name = seasonName

data Episode
  = Episode
    { episodeSeason :: Season
    , episodeName   :: Text
    , episodeIndex :: Int
    , episodeUrl    :: Url
    } deriving (Eq)

instance Show Episode where
  show (Episode t n i u) =  show $
    (showName . seasonShow $ t) <> " > " <> seasonName t
                                <> " > " <> tshow i <> ". " <> n
                                         <> " (" <> u <> ")"

instance HasName Episode where
  name = episodeName
