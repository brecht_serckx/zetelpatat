{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase       #-}
{-# LANGUAGE DeriveGeneric       #-}
module ZetelPatat.Provider.VrtNu where

import           RIO                     hiding ( logDebug
                                                , logError
                                                , logInfo
                                                , logOther
                                                , logWarn
                                                , unpack
                                                )
import qualified RIO.Text                      as T

import           Control.Monad.Except
import           Control.Monad.Http             ( MonadHttp
                                                , Url
                                                )
import qualified Control.Monad.Http            as Http
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString.Lazy.Char8    as BS
                                                ( unpack
                                                , pack
                                                )
import           Text.HTML.TagSoup

import           ZetelPatat.Types
import           ZetelPatat.Utils



data VrtNuProvider = VrtNuProvider
  deriving (Eq, Show)

instance Provider VrtNuProvider where
  providerName p = T.pack "vrtnu"

  -- | List all shows of a provider
  listShows p = parseShows =<< Http.get (T.pack "/vrtnu/a-z")

  -- | List all seasons of a show
  listSeasons p s = parseSeasons s =<< Http.get (showUrl s)

  -- | List all episodes of a season
  listEpisodes p = parseEpisodes
                  -- =<< Http.get (seasonUrl t)


parseShows :: MonadError AppError m => LByteString -> m [TVShow]
parseShows =
  mapM
      ( (\s -> TVShow (parseTitle s) <$> parseUrl s)
      . takeWhile (~/= "</a>")
      . dropWhile (~/= "<a>")
      )
  -- . (\x -> trace (tshow x) x)
    . partitions (~== "<nui-tile>")
    . takeWhile (~/= "</nui-list--content>")
    . dropWhile (~/= "<nui-list--content>")
    . parseTags
    . BS.unpack
 where
  parseTitle :: [Tag String] -> Text
  parseTitle = T.pack . innerText
  parseUrl :: MonadError AppError m => [Tag String] -> m Text
  parseUrl s = T.pack . fromAttrib "href" <$> case s of
    []    -> throwError . ParseError $ T.pack "Cannot parse shows"
    x : _ -> pure x



parseSeasons :: MonadError AppError m => TVShow -> LByteString -> m [Season]
parseSeasons show =
  mapM
      ( (\s -> Season show <$> parseTitle s <*> parseUrl s)
      . takeWhile (~/= "</a>")
      . dropWhile (~/= "<a>")
      )
  -- . (\x -> trace (tshow x) x)
    . partitions (~== "<li>")
    . takeWhile (~/= "</ul>")
    . dropWhile (~/= "<ul id=seasons-list>")
    . dropWhile (~/= "<div class=episodeslist>")
    . parseTags
    . BS.unpack
 where
  parseTitle :: MonadError AppError m => [Tag String] -> m Text
  parseTitle =
    fmap (T.strip . T.pack . fromTagText)
      . (\case
          x : _ -> pure x
          _     -> throwError . ParseError $ T.pack "Cannot parse seasons"
        )
      . dropWhile (not . isTagText)
      . dropWhile (~/= "<nui-replace-text")
  parseUrl :: MonadError AppError m => [Tag String] -> m Text
  parseUrl s =
    \case
        "#" -> showUrl show
        url -> T.pack url
      .   fromAttrib "href"
      <$> case s of
            []    -> throwError . ParseError $ T.pack "Cannot parse seasons"
            x : _ -> pure x


parseEpisodes :: (MonadError AppError m, MonadHttp m) => Season -> m [Episode]
parseEpisodes season =
  mapM
      ( (\s -> do
          title  <- parseTitle s
          url    <- parseUrl s
          epInfo <- parseEpisodeInfo url
          return $ Episode season title (episodeNumber epInfo) url
        )
      . takeWhile (~/= "</a>")
      . dropWhile (~/= "<a>")
      )
  -- . (\x -> trace (T.unlines $ map tshow x) x)
    .   partitions (~== "<li>")
    .   takeWhile (~/= "</ul>")
    .   takeWhile (~/= "</nui-list--content>")
    .   dropWhile (~/= "<nui-list--content>")
    .   dropWhile (~/= "<nui-list id=episodes-list>")
    .   parseTags
    .   BS.unpack
    =<< Http.get (seasonUrl season)
 where
  parseTitle :: MonadError AppError m => [Tag String] -> m Text
  parseTitle =
    fmap (T.strip . T.pack . fromTagText)
      . (\case
          x : _ -> pure x
          _     -> throwError . ParseError $ T.pack "Cannot parse episode title"
        )
      -- . (\x -> trace (T.unlines $ map tshow x) x)
      . dropWhile (not . isTagText)
  parseUrl :: MonadError AppError m => [Tag String] -> m Text
  parseUrl s =
    \case
        "#" -> seasonUrl season
        url -> T.pack url
      .   fromAttrib "href"
      <$> case s of
            []    -> throwError . ParseError $ T.pack "Cannot parse episodes"
            x : _ -> pure x

newtype EpisodeInfo
  = EpisodeInfo
  { episodeNumber :: Int
  } deriving (Generic)

parseEpisodeInfo
  :: (MonadError AppError m, MonadHttp m) => Url -> m EpisodeInfo
parseEpisodeInfo episodeUrl = do
  tags <- parseTags . BS.unpack <$> Http.get episodeUrl
  let json =
        map (T.strip . T.pack . fromTagText)
          . filter isTagText
          . takeWhile (~/= "</script>")
          . dropWhile (~/= "<script type=application/ld+json>")
          $ tags
  json' <- case json of
    [x] -> return x
    _   -> throwError . ParseError $ T.pack "Cannot parse episode info"
  parseEpisodeInfoJson json'

instance FromJSON EpisodeInfo

parseEpisodeInfoJson :: (MonadError AppError m) => Text -> m EpisodeInfo
parseEpisodeInfoJson =
  \case
      Nothing -> throwError . ParseError $ T.pack "Cannot parse episode info"
      Just x  -> pure x
    . decode
    . BS.pack
    . T.unpack

